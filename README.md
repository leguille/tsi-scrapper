# TSI sports scrapper

I have to consult this website for my son's soccer matches. Simply put, this website has terrible user experience. It was no surprise that the rendered html was as bad as what the pages looked like.

Goal is to have a `python` script use `bs4` to parse and get the data to `json` format and then generate a simple html table with the data while making it accessible for phones/tablets.

## Results

https://leguille.gitlab.io/tsi-scrapper

Generated twice daily (at noon and midnight).

## dev

* Install `python` and `pip`.
* Run `pip install beautifulsoup4`
* Run `python scrape.py`, you end up with a `*.json` file containing the current data and a `.html` file with the rendered code.

## todo

make it templatable for other teams/schedules.