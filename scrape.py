from bs4 import BeautifulSoup
from datetime import datetime
import json
import urllib.request
import urllib.parse

url = 'http://www.tsisports.ca/soccer/ligue/l_cal3.aspx?ch=HOR&lng=fr&valid=qa2mkdb1qxjsfy2fccdhiose'

def makeRequest(url, data=None):
    if data:
        with urllib.request.urlopen(url, data=data) as f:
            return BeautifulSoup(f.read(),"html5lib")
    else:
        with urllib.request.urlopen(url) as f:
            return BeautifulSoup(f.read(),"html5lib")

# gives us the inital form where I can "filter" the params to get the team DDL
page = makeRequest(url)

viewstate = page.select("#__VIEWSTATE")[0]['value']
viewstategen = page.select("#__VIEWSTATEGENERATOR")[0]['value']
eventvalidation = page.select("#__EVENTVALIDATION")[0]['value']

formData = (
    ('__VIEWSTATE', viewstate),
    ('__VIEWSTATEGENERATOR', viewstategen),
    ('__EVENTVALIDATION', eventvalidation),
    ('ctl00$ContentPlaceHolder1$cmbSex', ''),
    ('ctl00$ContentPlaceHolder1$cmbClass', ''),
    ('ctl00$ContentPlaceHolder1$cmbCateg', 'U-12'),
    ('ctl00$ContentPlaceHolder1$cmdRech', 'Rechercher'),
    ('__EVENTTARGET', ''),
    ('__EVENTARGUMENT', '')
)

encodedData = urllib.parse.urlencode(formData).encode("utf-8")

# gives me the page with the team DDL
pageTeamDDL = makeRequest(url, encodedData)

viewstate = pageTeamDDL.select("#__VIEWSTATE")[0]['value']
viewstategen = pageTeamDDL.select("#__VIEWSTATEGENERATOR")[0]['value']
eventvalidation = pageTeamDDL.select("#__EVENTVALIDATION")[0]['value']

formData = (
    ('__VIEWSTATE', viewstate),
    ('__VIEWSTATEGENERATOR', viewstategen),
    ('__EVENTVALIDATION', eventvalidation),
    ('ctl00$ContentPlaceHolder1$cmbSex', ''),
    ('ctl00$ContentPlaceHolder1$cmbClass', ''),
    ('ctl00$ContentPlaceHolder1$cmbCateg', 'U-12'),
    #CR U12M CDSO (2010/ 2009) MÉTÉORES (KI) (M U-12 A/Régionale)
    ('ctl00$ContentPlaceHolder1$cmbTeam', '7001110-2021'),
    ('__EVENTTARGET', 'ctl00$ContentPlaceHolder1$cmbTeam'),
    ('__EVENTARGUMENT', '')
)

encodedData = urllib.parse.urlencode(formData).encode("utf-8")

# Finally, gives us the team schedule ...
schedule = makeRequest(url, encodedData).table
rows = schedule.find_all("tr")

index = 0
list = []
listCalendar = []
listCalendar.append('Subject,Start Date,Start Time,End Date,End Time,Description,Location')

for row in rows:
    # tsi website is massive garbage, worst tables I have ever seen in my life.
    if row:
        the_column = row.find("td")
        if the_column:

            iMatchId = the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_lblMatch_'+str(index)).text
            iMatchInfo = ''
            if the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_lnkMatch_'+str(index)):
                iMatchId = the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_lnkMatch_'+str(index)).text
                iMatchInfo = the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_lnkMatch_'+str(index))['href']
            iDate = the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_lblDate_'+str(index)).text
            iHour = the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_lblTime_'+str(index)).text
            iHasChanged = False
            if the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_lblDate_'+str(index)).findAll('font'):
                iHasChanged = True
            iTeamHome = the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_lblHome_'+str(index)).text
            iTeamHomeLogo = the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_imgHome_'+str(index))['src']
            iTeamVisitor = the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_lblVisit_'+str(index)).text
            iTeamVisitorLogo = the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_imgVisit_'+str(index))['src']
            iField = the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_lblFieldname_'+str(index)).text
            iFieldUrl = the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_lnkField_'+str(index))['href']

            score = ''
            if the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_lblRezH_'+str(index)):
                score = the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_lblRezH_'+str(index)).text
                score += " - " + the_column.find(id='ContentPlaceHolder1_gamesTeam1_viewGames_lblRezV_'+str(index)).text

            x = {
                "matchId" : iMatchId,
                "matchInfo" : iMatchInfo,
                "date" : iDate,
                "homeTeam" : iTeamHome,
                "visitorTeam" : iTeamVisitor,
                "homeLogoUrl" : iTeamHomeLogo,
                "visitorLogoUrl" : iTeamVisitorLogo,
                "hasDateChanged" : iHasChanged,
                "field" : iField,
                "fieldUrl" : iFieldUrl,
                "score" : score,
                "hour" : iHour
            }
            list.append(x)

            '''
            Subject,Start Date,Start Time,End Date,End Time,Description,Location
            Pratique du mardi,10/19/2021,6:45 PM,10/19/2021,8:00 PM,"Pratique de soccer, arrivez 30 minutes en avance","50 Chemin de la Savane, Gatineau, QC J8T 3N2"
            Pratique du jeudi,10/21/2021,6:45 PM,10/21/2021,8:00 PM,"Pratique de soccer, arrivez 30 minutes en avance","50 Chemin de la Savane, Gatineau, QC J8T 3N2"
            '''
            df = iDate[4:]
            df = datetime.strptime(df, '%Y-%m-%d').strftime('%m/%d/%y')
            listCalendar.append('"Match '+iMatchId+'",'+df+','+iHour+','+df+',,"'+ iTeamHome + ' VS ' + iTeamVisitor +'",' + iField)
            index += 1

filename = 'schedule-' + datetime.today().strftime('%Y-%m-%d-%H%M') + '.json'
with open(filename, 'w') as fout:
    json.dump(list , fout)

with open('meteores_calendar.csv', 'w') as f:
    for item in listCalendar:
        f.write("%s\n" % item)

# populate the index.html page with proper rows...
tbody = ''
for m in list:

    stopwords = ['CR', 'U12M', 'CDSO', '(2010/', 'CDSO-CR', '2009)' , '-']
    querywords = m['homeTeam'].split()
    querywords2 = m['visitorTeam'].split()

    resultwords  = [word for word in querywords if word not in stopwords]
    homeTeamParsed = ' '.join(resultwords)

    resultwords  = [word for word in querywords2 if word not in stopwords]
    visitorTeamParsed = ' '.join(resultwords)

    entry = []
    if bool(m['hasDateChanged']):
        entry += '<tr class="table-warning">'
    else:
        entry += '<tr>'
    entry += '<th scope="row">' + m['date'] + ' <span class="border border-dark rounded bg-info px-1">' + m['hour'] + '</span></th>'
    entry += '<td title="' + m['homeTeam'] + '">' + homeTeamParsed + '</td>'
    entry += '<th scope="row">' + m['score'] + '</th>'
    entry += '<td title="' + m['visitorTeam'] + '">' + visitorTeamParsed + '</td>'
    match = m['matchId']
    if m['matchInfo']:
        match = '<a href="http://www.tsisports.ca/soccer/ligue/' + m['matchInfo'] + '">' + m['matchId'] + '</a>'
    entry += '<td>' + match + ', <a href="http://www.tsisports.ca/soccer/ligue/' + m['fieldUrl'] + '" title="' + m['field'] + '">Lieu</a></tr>'

    tbody += ''.join(entry)

# load up index.html and replace the content and write the result to output.html
with open('index.html', 'r') as file:
    fcontent = file.read()

bodySoup = BeautifulSoup(fcontent, 'html.parser')

# replace the paragraph using `replace_with` method
bodySoup.tbody.append(BeautifulSoup(tbody, 'html.parser'))
bodySoup.find(id='date').replace_with(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))

# open another file for writing
with open('output.html', 'w') as fp:
    # write the current soup content
    fp.write(str(bodySoup))